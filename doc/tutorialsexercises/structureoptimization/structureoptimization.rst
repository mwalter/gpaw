.. _structureoptimization:

=================================
Structure optimization
=================================

.. toctree::
   :maxdepth: 1

   ../H2/optimization
   lattice_constants/lattice_constants
   stress/stress
