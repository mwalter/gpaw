.. _projects:

========
Projects
========

.. toctree::

    intro/intro
    catalysis/catalysis
    magnetism/magnetism
    machinelearning/machinelearning
    excitedstates/excitedstates
    batteries/batteries
    workflows/workflows
