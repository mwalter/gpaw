.. _platforms and architectures:

===========================
Platforms and architectures
===========================

Linux:

.. toctree::
   :maxdepth: 1

   gbar/gbar
   Linux/centos
   Linux/Fedora
   Linux/openSUSE
   Linux/ubuntu
   Linux/sophia
   Linux/Niflheim/Niflheim
   Linux/Niflheim/load
   Linux/Niflheim/build
   Linux/Niflheim/gpu
   Linux/SUNCAT/SUNCAT
   Linux/akka
   Linux/batman
   Linux/brown
   Linux/carbon_cnm
   Linux/davinci
   Linux/huygens
   Linux/jureca
   Linux/justus
   Linux/Juwels/juwels
   Linux/monolith
   Linux/nemo
   Linux/sepeli
   Linux/sun_chpc
   Linux/supernova
   Linux/uranus
   Linux/odyssey

WSL:

.. toctree::
   :maxdepth: 1

   wsl-ubuntu


MacOSX:

.. toctree::
   :maxdepth: 1

   MacOSX/homebrew
   MacOSX/anaconda

AIX:

.. toctree::
   :maxdepth: 1

   AIX/ibmsc
   AIX/jump
   AIX/seaborg

BGQ:

.. toctree::
   :maxdepth: 2

   BGQ/mira

BSD:

.. toctree::
   :maxdepth: 1

   BSD/FreeBSD

Bull:

.. toctree::
   :maxdepth: 1

   Bull/curie
   Bull/curie_gpu

Cray:

.. toctree::
   :maxdepth: 1

   Cray/lumi
   Cray/nersc_perlmutter
   Cray/louhi
   Cray/sisu
   Cray/hermit

HP:

.. toctree::
   :maxdepth: 1

   HP/xc

Solaris:

.. toctree::
   :maxdepth: 1

   Solaris/corona
